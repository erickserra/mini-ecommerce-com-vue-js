export function serialize(Obj) {
  let queryString = "";
  for (let key in Obj) {
    queryString += `&${key}=${Obj[key]}`;
  }
  return queryString;
}

export function mapFields(options) {
  const objecto = {}
  for (let x = 0; x < options.fields.length; x++) {
    const field = [options.fields[x]]
    objecto[field] = {
      get() {
        return this.$store.state.user[options.base][field]
      },
      set(value) {
        this.$store.commit(`${options.namespace}/${options.mutation}`, { [field]: value})
      }
    }
  }
  return objecto;
}
