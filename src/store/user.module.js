import {
  UPDATE_LOGIN,
  UPDATE_USUARIO,
  ADD_USUARIO_PRODUTOS, 
  UPDATE_USUARIO_PRODUTOS
 } from './mutations.type'
import { GET_USUARIO, 
  CREATE_USER, LOGOUT, 
  GET_USUARIO_PRODUTOS, 
  LOGIN_USER 
} from './actions.type'
import { ApiService } from '@/common/api.service'

const state = {
  strict: true,
  usuario: {
    id: "",
    nome: "",
    email: "",
    senha: "",
    cep: "",
    rua: "",
    numero: "",
    bairro: "",
    cidade: "",
    estado: ""
  },
  usuario_produtos: null,
  login: false
}

const mutations = {
  [UPDATE_LOGIN](state, payload) {
    state.login = payload
  },

  [UPDATE_USUARIO](state, payload) {
    state.usuario = Object.assign(state.usuario, payload)
  },

  [UPDATE_USUARIO_PRODUTOS](state, payload) {
    state.usuario_produtos = payload
  },

  [ADD_USUARIO_PRODUTOS](state, payload) {
    state.usuario_produtos.unshift(payload)
  }
}

const actions = {
  [GET_USUARIO](context) {
    return new Promise((resolve, reject) => {
      ApiService.get(`/usuario`)
        .then((response) => {
          context.commit(UPDATE_USUARIO, response.data)
          return response
        })
        .then((response) => {
          context.commit(UPDATE_LOGIN, true)
          resolve(response)
        })
        .catch(erro => {
          reject(erro)
        })
    })
  },
  [CREATE_USER](context, payload) {
    return new Promise((resolve, reject) => {
      payload.id = payload.email
      ApiService.post(`/usuario`, { ...payload }).then(() => {
        resolve()
      }).catch(() => {
        reject()
      })
    })
  },
  [GET_USUARIO_PRODUTOS](context) {
    return new Promise((resolve, reject) => {
      ApiService.get(`/produto?usuario_id=${context.state.usuario.id}`)
        .then(response => {
          context.commit(UPDATE_USUARIO_PRODUTOS, response.data)
          resolve()
        })
        .catch(() => {
          reject()
        })
    })
  },
  [LOGIN_USER] (context, payload) {
    console.log(payload)
    return ApiService.login({
      username: payload.email,
      password: payload.senha
    }).then(response => {
      window.localStorage.token = `Bearer ${response.data.token}`
    })
  },
  [LOGOUT](context) {
    return new Promise((resolve, reject) => {
      context.commit(UPDATE_USUARIO, {
        id: "",
        nome: "",
        email: "",
        senha: "",
        cep: "",
        rua: "",
        numero: "",
        bairro: "",
        cidade: "",
        estado: ""
      })
      window.localStorage.removeItem("token");
      context.commit(UPDATE_LOGIN, false)
      resolve()
    })
  },

}

const getters = {

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}