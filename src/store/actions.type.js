export const GET_USUARIO = 'getUsuario';
export const CREATE_USER = 'createUser';
export const LOGOUT = 'logoutUser';
export const GET_USUARIO_PRODUTOS = 'getUsuarioProdutos';
export const LOGIN_USER = `loginUser`;